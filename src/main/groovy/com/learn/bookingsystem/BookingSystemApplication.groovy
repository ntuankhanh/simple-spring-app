package com.learn.bookingsystem

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class BookingSystemApplication {

    static void main(String[] args) {
        SpringApplication.run(BookingSystemApplication, args)
    }

}
