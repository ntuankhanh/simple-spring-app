package com.learn.bookingsystem.crub.controllers

import com.learn.bookingsystem.crub.dto.MovieDTO
import com.learn.bookingsystem.crub.exceptions.ResourceNotFoundException
import com.learn.bookingsystem.crub.models.Movie
import com.learn.bookingsystem.crub.models.MovieRating
import com.learn.bookingsystem.crub.repositories.MovieRatingRepository
import com.learn.bookingsystem.crub.repositories.MovieRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

import javax.validation.Valid

@RestController
@RequestMapping("/api/v1/movies")
class MovieController {
    @Autowired
    private MovieRepository movieRepository

    @Autowired
    private MovieRatingRepository movieRatingRepository

    @GetMapping("")
    List<MovieDTO> getAllMovies() {
        List<MovieDTO> list = movieRepository.findAll().collect{movie -> new MovieDTO(movie)}
        return list
    }

    @GetMapping("/{id}")
    ResponseEntity<MovieDTO> getMovieById(@PathVariable(value = "id") Long MovieId)
            throws ResourceNotFoundException {
        Movie movie = movieRepository.findOne(MovieId as BigDecimal)
        MovieDTO mv = new MovieDTO(movie)
        return ResponseEntity.ok().body(mv)
    }

    @PostMapping("")
    Movie createMovie(@Valid @RequestBody Movie movie) {
        MovieRating movieRating = movieRatingRepository.findById(3 as BigDecimal)
                .orElseThrow{ -> new ResourceNotFoundException("Movie not found for this id :: " + 3) }
        movie.setMovieRating(movieRating)
        return movieRepository.save(movie)
    }

    @PutMapping("/{id}")
    ResponseEntity<Movie> updateMovie(@PathVariable(value = "id") Long movieId,
                                      @Valid @RequestBody Movie movieDetails) throws ResourceNotFoundException {
        Movie movie = movieRepository.findById(movieId as BigDecimal)
                .orElseThrow{ -> new ResourceNotFoundException("Movie not found for this id :: " + movieId) }
        movie.setName(movieDetails.getName())
        movie.setDescriptions(movieDetails.getDescriptions())
        movie.setAuthor(movieDetails.getAuthor())
        movie.setDuration(movieDetails.getDuration())
        movie.setTitle(movieDetails.getTitle())
        movie.setReleaseDate(movieDetails.getReleaseDate())
        movie.setCountry(movieDetails.getCountry())
        movie.setLanguage(movieDetails.getLanguage())
        final Movie updatedMovie = movieRepository.save(movie)
        return ResponseEntity.ok(updatedMovie)
    }

    @DeleteMapping("/{id}")
    Map<String, Boolean> deleteMovie(@PathVariable(value = "id") Long MovieId)
            throws ResourceNotFoundException {
        Movie Movie = movieRepository.findById(MovieId as BigDecimal)
                .orElseThrow{ -> new ResourceNotFoundException("Movie not found for this id :: " + MovieId) }

        movieRepository.delete(Movie)
        Map<String, Boolean> response = new HashMap<>()
        response.put("deleted", Boolean.TRUE)
        return response
    }
}
