package com.learn.bookingsystem.crub.models

import com.fasterxml.jackson.annotation.JsonManagedReference
import org.hibernate.annotations.Fetch
import org.hibernate.annotations.FetchMode

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.OneToMany
import javax.persistence.Table
import java.time.LocalDateTime

@Entity
@Table(name = "movie_rating")
class MovieRating extends BaseModel implements Serializable{

    @Column(name = "movie_rating_stars", nullable = false)
    private Integer movieRatingStart

    @Column(name = "movie_rating_description", nullable = true)
    private String ratingDescription

    @Column(name = "created_at", nullable = true)
    private LocalDateTime createdTime

    @Column(name = "updated_at", nullable = true)
    private LocalDateTime updatedTime

    @OneToMany(mappedBy = "movieRating" , fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonManagedReference
    private Collection<Movie> movies

    MovieRating(Integer movieRatingStart, String ratingDescription, LocalDateTime createdTime, LocalDateTime updatedTime) {
        this.movieRatingStart = movieRatingStart
        this.ratingDescription = ratingDescription
        this.createdTime = createdTime
        this.updatedTime = updatedTime
    }

    MovieRating(){

    }

    Integer getMovieRatingStart() {
        return movieRatingStart
    }

    void setMovieRatingStart(Integer movieRatingStart) {
        this.movieRatingStart = movieRatingStart
    }

    String getRatingDescription() {
        return ratingDescription
    }

    void setRatingDescription(String ratingDescription) {
        this.ratingDescription = ratingDescription
    }
}
