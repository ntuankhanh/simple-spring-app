package com.learn.bookingsystem.crub.models

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp

import javax.persistence.Column
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Inheritance
import javax.persistence.InheritanceType
import javax.persistence.MappedSuperclass
import javax.persistence.Version
import java.time.LocalDateTime

@MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
abstract class BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private BigDecimal id;

    @Column(name = "\"version\"", nullable = true)
    @Version
    private Integer version;

    @CreationTimestamp
    @Column(name = "created_at", nullable = false)
    private LocalDateTime createdTime

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    private LocalDateTime updatedTime

    @Column(name = "updatedBy", nullable = true)
    private BigDecimal updatedBy;

    BigDecimal getId() {
        return id;
    }

    void setId(BigDecimal id) {
        this.id = id;
    }

    LocalDateTime getCreatedTime() {
        return createdTime
    }

    void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime
    }

    LocalDateTime getUpdatedTime() {
        return updatedTime
    }

    void setUpdatedTime(LocalDateTime updatedTime) {
        this.updatedTime = updatedTime
    }

    BigDecimal getCreatedBy() {
        return createdBy;
    }

    void setCreatedBy(BigDecimal createdBy) {
        this.createdBy = createdBy;
    }
    
    BigDecimal getUpdatedBy() {
        return updatedBy;
    }

    void setUpdatedBy(BigDecimal updatedBy) {
        this.updatedBy = updatedBy;
    }

    Integer getVersion() {
        return version;
    }

    void setVersion(Integer version) {
        this.version = version;
    }
}