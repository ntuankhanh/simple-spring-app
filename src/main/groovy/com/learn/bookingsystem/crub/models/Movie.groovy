package com.learn.bookingsystem.crub.models

import com.fasterxml.jackson.annotation.JsonBackReference

import javax.persistence.*
import java.time.LocalDateTime

@SuppressWarnings("serial")
@Entity
@Table(name = "movie")
class Movie extends BaseModel implements Serializable {

    @Column(name = "movie_name", nullable = false)
    private String name

    @Column(name = "movie_descriptions", nullable = false)
    private String descriptions

    @Column(name = "movie_author", nullable = false)
    private String author

    @Column(name = "movie_release_date", nullable = false)
    private LocalDateTime releaseDate

    @Column(name = "movie_duration", nullable = false)
    private Integer duration

    @Column(name = "movie_language", nullable = false)
    private String language

    @Column(name = "movie_title", nullable = false)
    private String title

    @Column(name = "movie_country", nullable = false)
    private String country

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fk_movie_rating_id", referencedColumnName = "id")
    @JsonBackReference
    private MovieRating movieRating;

    Movie(String descriptions, String author, LocalDateTime releaseDate, Integer duration, String language, String title, String country) {
        this.descriptions = descriptions
        this.author = author
        this.releaseDate = releaseDate
        this.duration = duration
        this.language = language
        this.title = title
        this.country = country
    }

    Movie(){

    }

    String getName() {
        return name
    }

    void setName(String name) {
        this.name = name
    }

    String getDescriptions() {
        return descriptions
    }

    void setDescriptions(String descriptions) {
        this.descriptions = descriptions
    }

    String getAuthor() {
        return author
    }

    void setAuthor(String author) {
        this.author = author
    }

    LocalDateTime getReleaseDate() {
        return releaseDate
    }

    void setReleaseDate(LocalDateTime releaseDate) {
        this.releaseDate = releaseDate
    }

    Integer getDuration() {
        return duration
    }

    void setDuration(Integer duration) {
        this.duration = duration
    }

    String getLanguage() {
        return language
    }

    void setLanguage(String language) {
        this.language = language
    }

    String getTitle() {
        return title
    }

    void setTitle(String title) {
        this.title = title
    }

    String getCountry() {
        return country
    }

    void setCountry(String country) {
        this.country = country
    }

    MovieRating getMovieRating() {
        return movieRating
    }

    void setMovieRating(MovieRating movieRating) {
        this.movieRating = movieRating
    }
}
