package com.learn.bookingsystem.crub.models

import com.learn.bookingsystem.crub.dto.IDto

interface IEntity {
    void updateFields(IDto dto);

    IDto convert2Dto();
}