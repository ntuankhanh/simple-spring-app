package com.learn.bookingsystem.crub.dto

import com.learn.bookingsystem.crub.models.IEntity
import com.learn.bookingsystem.crub.models.Movie

import javax.persistence.Column
import java.time.LocalDateTime

class MovieDTO implements IDto{
    private BigDecimal id
    private String name
    private String descriptions
    private String author
    private LocalDateTime releaseDate
    private Integer duration
    private String language
    private String title
    private String country
    private MovieRatingDTO movieRatingDTO

    MovieDTO(BigDecimal id, String name, String descriptions, String author, LocalDateTime releaseDate,
             Integer duration, String language, String title, String country, MovieRatingDTO movieRatingDTO) {
        this.id = id
        this.name = name
        this.descriptions = descriptions
        this.author = author
        this.releaseDate = releaseDate
        this.duration = duration
        this.language = language
        this.title = title
        this.country = country
        this.movieRatingDTO = movieRatingDTO
    }

    MovieDTO(Movie movie){
        this.id = movie.getId()
        this.name = movie.getName()
        this.descriptions = movie.getDescriptions()
        this.author = movie.getAuthor()
        this.releaseDate = movie.getReleaseDate()
        this.duration = movie.getDuration()
        this.language = movie.getLanguage()
        this.title = movie.getTitle()
        this.country = movie.country
        this.movieRatingDTO = new MovieRatingDTO(movie.getMovieRating());
    }
    BigDecimal getId() {
        return id
    }

    void setId(BigDecimal id) {
        this.id = id
    }

    String getName() {
        return name
    }

    void setName(String name) {
        this.name = name
    }

    String getDescriptions() {
        return descriptions
    }

    void setDescriptions(String descriptions) {
        this.descriptions = descriptions
    }

    String getAuthor() {
        return author
    }

    void setAuthor(String author) {
        this.author = author
    }

    LocalDateTime getReleaseDate() {
        return releaseDate
    }

    void setReleaseDate(LocalDateTime releaseDate) {
        this.releaseDate = releaseDate
    }

    Integer getDuration() {
        return duration
    }

    void setDuration(Integer duration) {
        this.duration = duration
    }

    String getLanguage() {
        return language
    }

    void setLanguage(String language) {
        this.language = language
    }

    String getTitle() {
        return title
    }

    void setTitle(String title) {
        this.title = title
    }

    String getCountry() {
        return country
    }

    void setCountry(String country) {
        this.country = country
    }

    MovieRatingDTO getMovieRatingDTO() {
        return movieRatingDTO
    }

    void setMovieRatingDTO(MovieRatingDTO movieRatingDTO) {
        this.movieRatingDTO = movieRatingDTO
    }

    @Override
    void updateDTO(IEntity entry) {
        Movie movieEntity = (Movie) entry

    }
}
