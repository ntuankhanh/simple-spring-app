package com.learn.bookingsystem.crub.dto

import com.learn.bookingsystem.crub.models.MovieRating

import javax.persistence.Column
import java.time.LocalDateTime

class MovieRatingDTO {
    private BigDecimal id
    private Integer movieRatingStart
    private String ratingDescription
    private LocalDateTime createdTime
    private LocalDateTime updatedTime

    MovieRatingDTO(BigDecimal id, Integer movieRatingStart, String ratingDescription, LocalDateTime createdTime, LocalDateTime updatedTime) {
        this.id = id
        this.movieRatingStart = movieRatingStart
        this.ratingDescription = ratingDescription
        this.createdTime = createdTime
        this.updatedTime = updatedTime
    }

    MovieRatingDTO(MovieRating movieRating){
        this.id = movieRating.getId()
        this.movieRatingStart = movieRating.getMovieRatingStart()
        this.ratingDescription = movieRating.getRatingDescription()
        this.createdTime = movieRating.getCreatedTime()
        this.updatedTime = movieRating.getUpdatedTime()
    }

    BigDecimal getId() {
        return id
    }

    void setId(BigDecimal id) {
        this.id = id
    }

    Integer getMovieRatingStart() {
        return movieRatingStart
    }

    void setMovieRatingStart(Integer movieRatingStart) {
        this.movieRatingStart = movieRatingStart
    }

    String getRatingDescription() {
        return ratingDescription
    }

    void setRatingDescription(String ratingDescription) {
        this.ratingDescription = ratingDescription
    }

    LocalDateTime getCreatedTime() {
        return createdTime
    }

    void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime
    }

    LocalDateTime getUpdatedTime() {
        return updatedTime
    }

    void setUpdatedTime(LocalDateTime updatedTime) {
        this.updatedTime = updatedTime
    }
}
