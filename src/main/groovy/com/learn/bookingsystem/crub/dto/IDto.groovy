package com.learn.bookingsystem.crub.dto

import com.learn.bookingsystem.crub.models.IEntity

interface IDto {
    void updateDTO(IEntity entry);
}