package com.learn.bookingsystem.crub.repositories

import com.learn.bookingsystem.crub.models.Movie
import org.springframework.stereotype.Repository

@Repository
class MovieRepository extends BaseDao<Movie, BigDecimal> {
    MovieRepository(){
        this.setClazz(Movie.class)
    }
}
