package com.learn.bookingsystem.crub.repositories

import com.learn.bookingsystem.crub.models.MovieRating
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
public interface MovieRatingRepository extends JpaRepository<MovieRating, BigDecimal> {
}
