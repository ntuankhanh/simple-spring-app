package com.learn.bookingsystem.crub.repositories

import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

abstract class BaseDao< T extends Serializable, K >{
    private Class< T > clazz;

    @PersistenceContext
    EntityManager entityManager;

    final void setClazz( Class< T > clazzToSet ){
        this.clazz = clazzToSet;
    }

    T findOne( K id ){
        return entityManager.find( clazz, id );
    }
    List< T > findAll(){
        return entityManager.createQuery( "from " + clazz.getName() )
                .getResultList();
    }

    void create( T entity ){
        entityManager.persist( entity );
    }

    T update( T entity ){
        return entityManager.merge( entity );
    }

    void delete( T entity ){
        entityManager.remove( entity );
    }
    void deleteById( K entityId ){
        T entity = findOne( entityId );
        delete( entity );
    }
}